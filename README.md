# Тестовое задание на позицию Младший бекенд разработчик

Стек:
- Java17
- SpringBoot
- SpringData JPA
- Flyway
- PostgreSQL
- Docker

Тестовое задание:

- [x] Поднять PostgreSQL и создать таблички для колес, кузовов и машин
  - [x] Использовать Flyway для миграций + заполнить табличку дефолтными значениями - [code](src/main/resources/db/migration)
- [x] Создать ручку для getAllCarBody - [code](src/main/java/ru/bluebasooo/carservice/controller/BodyController.java)
- [x] Создать ручку для getAllCarWheels - [code](src/main/java/ru/bluebasooo/carservice/controller/WheelController.java)
- [x] Создать ручку для createCar - [code](src/main/java/ru/bluebasooo/carservice/controller/CarController.java)
  - [x] Добавить проверку существования id колес и кузовов
- [x] Создать ручку для getCarById - [code](src/main/java/ru/bluebasooo/carservice/controller/CarController.java)
- [x] Создать ручку getCars предусматривающая пагинацию - [code](src/main/java/ru/bluebasooo/carservice/controller/CarController.java)

Доп задания:

- [ ] Использовать кеширование запросов
- [x] Запаковать приложение в докер контейнер - [code](Dockerfile)
- [x] Использовать docker-compose - [code](docker-compose.yml)

По собственной инициативе:

- [x] Добавлена обработка ошибок - [code](src/main/java/ru/bluebasooo/carservice/exception)
- [x] Добавлен Swagger - [ссылка](http://localhost:8080/api/docs/ui)

