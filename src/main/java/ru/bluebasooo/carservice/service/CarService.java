package ru.bluebasooo.carservice.service;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.bluebasooo.carservice.dto.CreatingCarDto;
import ru.bluebasooo.carservice.exception.BadRequestParamsException;
import ru.bluebasooo.carservice.model.Car;
import ru.bluebasooo.carservice.repository.CarRepository;

@Service
@AllArgsConstructor
public class CarService {

    private CarRepository carRepository;
    private BodyService bodyService;
    private WheelService wheelService;

    public Car createCar(CreatingCarDto carDto) {
        var wheel = wheelService.getWheelById(carDto.getWheelId());
        var body = bodyService.getBodyById(carDto.getBodyId());

        var carToSave = Car.builder()
                .name(carDto.getName())
                .body(body)
                .wheel(wheel)
                .numOfWheels(carDto.getNumOfWheels())
                .build();

        return carRepository.save(carToSave);
    }


    public Car getCarById(Long id) {
        return carRepository.findById(id)
                .orElseThrow(() -> new BadRequestParamsException("Car with current id not found"));
    }


    public Page<Car> getCars(Pageable pageable) {
        return carRepository.findAll(pageable);
    }
}
