package ru.bluebasooo.carservice.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.bluebasooo.carservice.exception.BadRequestParamsException;
import ru.bluebasooo.carservice.model.Wheel;
import ru.bluebasooo.carservice.repository.WheelRepository;

import java.util.List;

@Service
@AllArgsConstructor
public class WheelService {

    private WheelRepository wheelRepository;
    public List<Wheel> getAllCarWheels() {
        return wheelRepository.findAll();
    }

    public Boolean isWheelExists(Long id) {
        return wheelRepository.existsById(id);
    }

    public Wheel getWheelById(Long wheelId) {
        return wheelRepository.findById(wheelId)
                .orElseThrow(() -> new BadRequestParamsException("Wheel with current id doest not exists"));
    }
}
