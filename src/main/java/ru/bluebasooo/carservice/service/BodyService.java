package ru.bluebasooo.carservice.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.bluebasooo.carservice.exception.BadRequestParamsException;
import ru.bluebasooo.carservice.model.Body;
import ru.bluebasooo.carservice.repository.BodyRepository;

import java.util.List;

@Service
@AllArgsConstructor
public class BodyService {

    private BodyRepository bodyRepository;

    public List<Body> getAllCarBody() {
        return bodyRepository.findAll();
    }

    public Boolean isBodyExists(Long id) {
        return bodyRepository.existsById(id);
    }

    public Body getBodyById(Long bodyId) {
        return bodyRepository.findById(bodyId)
                .orElseThrow(() -> new BadRequestParamsException("Body with current id does not exists"));
    }
}
