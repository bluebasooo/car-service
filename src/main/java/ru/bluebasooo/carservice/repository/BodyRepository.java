package ru.bluebasooo.carservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bluebasooo.carservice.model.Body;

@Repository
public interface BodyRepository extends JpaRepository<Body, Long> {
}
