package ru.bluebasooo.carservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bluebasooo.carservice.model.Wheel;

@Repository
public interface WheelRepository extends JpaRepository<Wheel, Long> {
}
