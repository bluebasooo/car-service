package ru.bluebasooo.carservice.exception;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;


@Getter
@Builder
@AllArgsConstructor
public class ApiError {
    String message;
}
