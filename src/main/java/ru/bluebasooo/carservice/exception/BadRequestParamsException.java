package ru.bluebasooo.carservice.exception;

public class BadRequestParamsException extends RuntimeException {
    public BadRequestParamsException(String message) {
        super(message);
    }
}
