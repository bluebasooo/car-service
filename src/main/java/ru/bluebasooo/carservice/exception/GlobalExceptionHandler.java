package ru.bluebasooo.carservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(BadRequestParamsException.class)
    public ResponseEntity<ApiError> catchBadRequestParamsException(BadRequestParamsException e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND.value())
                .body(new ApiError(e.getMessage()));
    }
}
