package ru.bluebasooo.carservice.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class CreatingCarDto {
    String name;
    Long wheelId;
    Long bodyId;
    Integer numOfWheels;
}
