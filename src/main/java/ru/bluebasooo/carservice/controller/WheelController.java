package ru.bluebasooo.carservice.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.bluebasooo.carservice.model.Wheel;
import ru.bluebasooo.carservice.service.WheelService;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/wheel")
@Tag(name = "Wheel", description = "Controller for wheels operations")
public class WheelController {

    private WheelService wheelService;

    @GetMapping("/getAllCarWheels")
    public List<Wheel> getAllCarWheels() {
        return wheelService.getAllCarWheels();
    }

}
