package ru.bluebasooo.carservice.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.bluebasooo.carservice.dto.CreatingCarDto;
import ru.bluebasooo.carservice.model.Car;
import ru.bluebasooo.carservice.service.CarService;

@RestController
@AllArgsConstructor
@RequestMapping("/car")
@Tag(name = "Car", description = "Controller for cars operations")
public class CarController {

    private CarService carService;

    @PostMapping("/createCar")
    public ResponseEntity<Car> createCar(@RequestBody CreatingCarDto carDto) {
        return ResponseEntity.ok(carService.createCar(carDto));
    }

    @GetMapping("/getCarById/{id}")
    public ResponseEntity<Car> getCarById(@PathVariable Long id) {
        return ResponseEntity.ok(carService.getCarById(id));
    }

    @GetMapping("/getCars")
    public Page<Car> getCars(@PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return carService.getCars(pageable);
    }

}
