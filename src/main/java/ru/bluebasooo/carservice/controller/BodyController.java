package ru.bluebasooo.carservice.controller;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.bluebasooo.carservice.model.Body;
import ru.bluebasooo.carservice.service.BodyService;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/body")
@Tag(name="Body", description = "Controller for car's body operations")
public class BodyController {

    private BodyService bodyService;

    @GetMapping("/getAllCarBody")
    public List<Body> getAllCarBody() {
        return bodyService.getAllCarBody();
    }

}
