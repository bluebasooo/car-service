package ru.bluebasooo.carservice.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "car")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;

    @Column(name = "name")
    String name;

    @Column(name = "num_wheels")
    Integer numOfWheels;

    @OneToOne
    @JoinColumn(name = "body_id", referencedColumnName = "id")
    Body body;

    @OneToOne
    @JoinColumn(name = "wheel_id", referencedColumnName = "id")
    Wheel wheel;
}
