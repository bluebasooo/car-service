package ru.bluebasooo.carservice.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "car_body")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Body {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "body_type")
    String type;
}
