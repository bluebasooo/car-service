package ru.bluebasooo.carservice.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "car_wheel")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Wheel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    Integer radius;
}
