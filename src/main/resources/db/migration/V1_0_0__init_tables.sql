CREATE TABLE IF NOT EXISTS "car_wheel" (
    "id" BIGSERIAL PRIMARY KEY,
    "radius" INTEGER UNIQUE
);

CREATE TABLE IF NOT EXISTS "car_body" (
    "id" BIGSERIAL PRIMARY KEY,
    "body_type" VARCHAR(20) UNIQUE
);

CREATE TABLE IF NOT EXISTS "car" (
    "id" BIGSERIAL PRIMARY KEY,
    "name" VARCHAR(40) NOT NULL,
    "num_wheels" INT NOT NULL,
    "wheel_id" BIGINT,
    "body_id" BIGINT,

    FOREIGN KEY("wheel_id") REFERENCES car_wheel("id"),
    FOREIGN KEY("body_id") REFERENCES car_body("id")
);