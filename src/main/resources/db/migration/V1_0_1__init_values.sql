INSERT INTO "car_wheel"("radius") VALUES
(15), (16), (17)
ON CONFLICT DO NOTHING;

INSERT INTO "car_body"("body_type") VALUES
('hatchback'), ('sedan')
ON CONFLICT DO NOTHING;
